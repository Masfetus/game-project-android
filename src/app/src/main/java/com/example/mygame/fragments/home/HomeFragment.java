package com.example.mygame.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mygame.R;
import com.example.mygame.activities.HomeActivity;
import com.example.mygame.activities.GameActivity;
import com.example.mygame.activities.RulesActivity;
import com.example.mygame.game.Player;
import com.google.android.material.tabs.TabLayout;

public class HomeFragment extends Fragment {

    private HomeActivity homeActivity;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        homeActivity = (HomeActivity) getContext();
        homeActivity.getSupportActionBar().hide();
        return root;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView tvPseudo = homeActivity.findViewById(R.id.tvPlayerPseudo), tvBitcoins = homeActivity.findViewById(R.id.tvPlayerBitcoins);
        final Player currentPlayer =  homeActivity.getCurrentPlayer();
        final TabLayout tlLevels = homeActivity.findViewById(R.id.tabLayoutLevels);

        tvPseudo.setText(currentPlayer.toString());
        tvBitcoins.setText("" + currentPlayer.getBitCoins());
        (homeActivity.findViewById(R.id.btnPlay)).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), GameActivity.class);
            intent.putExtra("player1", currentPlayer);
            intent.putExtra("difficultyLevel", tlLevels.getSelectedTabPosition() + 1);
            homeActivity.startActivity(intent);
            homeActivity.finish();
        });
        (homeActivity.findViewById(R.id.btnRules)).setOnClickListener(v -> {
            startActivity(new Intent(getContext(), RulesActivity.class));
        });
    }
    @Override
    public void onStop() {
        super.onStop();
        homeActivity.getSupportActionBar().show();
    }
}
