package com.example.mygame.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.mygame.R;

public class RulesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);
        setFinishOnTouchOutside(true);
    }
}
