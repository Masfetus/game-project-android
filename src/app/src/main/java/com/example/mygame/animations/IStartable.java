package com.example.mygame.animations;
@FunctionalInterface
public interface IStartable {
    void start();
}
