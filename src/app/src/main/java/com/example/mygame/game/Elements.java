package com.example.mygame.game;

import java.util.HashMap;
import java.util.Map;

public enum Elements {
    ROCK(0),
    LEAF(1),
    SCISSORS(2),
    LIZARD(3),
    SPOCK(4);
    private int value;
    private static Map map = new HashMap<>();

    Elements(int value) {
        this.value = value;
    }
    static {
        for (Elements element : Elements.values()) {
            map.put(element.value, element);
        }
    }
    public int getValue() { return value; } // Get ID of an Element

    public static Elements valueOf(int pageType) { // Get Elements from an ID
        return (Elements) map.get(pageType);
    }
}
