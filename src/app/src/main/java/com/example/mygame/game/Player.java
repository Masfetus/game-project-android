package com.example.mygame.game;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.NonNull;

import com.example.mygame.activities.HomeActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Player implements Serializable, ISavable {
    /*
        Class Player : represents a player in the game (IA or User)
        - Attributes :
            String encodedPassword      :         Password of the user (encoded)
            String login                :         Login of the user
            int age                     :         Age of the user
            int bitCoins                :         Amount of bitcoins of the user
            String pseudo               :         Username of the player in the game
            int lastScore               :         Score of the player at the last round
            Elements lastElementPlayed  :         Element played by the player at the last round
        - Constructors :
            Player(int, String, String, String) : instances the class with an age, username, login and encodedPassword
            Player()                            : default construct used by Firebase to retrieve data
        - Getters :
            String getLogin()     :               returns value of login
            String getPseudo()    :               returns value of pseudo
            int getBitCoins()     :               returns value of bitCoins
            int getLastScore()    :               returns value of lastScore
            Elements getLastElementPlayed() :     returns value of lastElementPlayed
        - Setters :
            void setLastElementPlayed(Elements)     :     sets the lastElementPlayed to the value
            void setBitCoins(int)                   :     sets the bitCoins to the value (by staying positive)
            void setPseudo(String)                  :     sets the pseudo to the value (if value is non null)
            void setLastScore(int)                  :     sets the lastScore to the value (by staying positive)
        - Override methods :
            void save()                             :   stores the user data in Firebase
            String toString()                       :   returns the player pseudo with hashtang at the beginning
        - Method :
            public void giveBitcoins(int)           :   give amount of bitcoins to player (negative or positive value)
        - Static methods :
            public String encodePasswordSha512(String)                 :   returns the password passed in parameters encoded
            public boolean isNetworkAvailable(Activity)                :   determines if network is available on the passed activity
            public void createPlayerFromDatabase(final Context, String):   loads data (from Firebase) and calls the constructor of Player thanks to the login passed
     */
    private String encodedPassword;
    private String login;
    private long uID;
    private int age;
    private int bitCoins;
    private String pseudo;
    private int lastScore;



    private Elements lastElementPlayed;

    public Player(int age, String pseudo, String login, String encodedPassword)
    {
        this.login = (login == null ? "" : login);
        this.encodedPassword = (encodedPassword == null ? "" : encodedPassword);
        setBitCoins(0);
        this.age = age;
        setPseudo(pseudo);
    }
    public Player(){ this(0, null, null, null); }

    // Getters
    public String getEncodedPassword() { return encodedPassword; }
    public long getuID() { return uID; }
    public String getLogin() { return login; }
    public String getPseudo() { return pseudo; }
    public int getBitCoins() { return bitCoins; }
    public int getLastScore() { return lastScore; }
    public Elements getLastElementPlayed() { return lastElementPlayed; }


    // Setters
    public void setuID(long uID){
        if(uID >= 0)
            this.uID = uID;
    }
    public void setLastElementPlayed(Elements lastElementPlayed) {
        this.lastElementPlayed = lastElementPlayed;
    }
    public void setBitCoins(int bitCoins){
        this.bitCoins = (bitCoins < 0 ? 0 : bitCoins);
    }
    public void setPseudo(String pseudo) {
        if(pseudo != null)
            this.pseudo = pseudo;
    }
    public void setLastScore(int score) {
            lastScore = (score < 0 ? 0 : score);
    }
    // Methods
    @Override
    public void save()
    {
        if(this != null) {
            FirebaseDatabase firebaseInstance = FirebaseDatabase.getInstance();
            firebaseInstance.getReference("users/" + uID).setValue(this);
        }
    }

    public void giveBitcoins(int amount){
        setBitCoins(bitCoins + amount);
    }

    // Static methods
    public static String encodePasswordSha512(String password)
    {
        MessageDigest digest = null;
        String encodedPassword;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] resDigest = digest.digest(password.getBytes());
        BigInteger signumRepresentedPassword = new BigInteger(1, resDigest);
        encodedPassword = signumRepresentedPassword.toString(16);

        while (encodedPassword.length() < 32) {
            encodedPassword = "0" + encodedPassword;
        }
        return encodedPassword;
    }
    public static boolean isNetworkAvailable(Activity playerActivity) {
        if(playerActivity == null)
            return false;
        ConnectivityManager connectivityManager = (ConnectivityManager) playerActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static void createPlayerFromDatabase(final Context currentContext, long uID)
    {
            FirebaseDatabase.getInstance().getReference("users/"+ uID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Player newPlayer = dataSnapshot.getValue(Player.class);
                Intent intent = new Intent(currentContext, HomeActivity.class);
                intent.putExtra("player", newPlayer);
                currentContext.startActivity(intent);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError);
            }
        });
    }
    @Override
    public String toString() {
        return "#" + pseudo;
    }
}
