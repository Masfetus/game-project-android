package com.example.mygame.animations;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.renderscript.Sampler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.constraintlayout.solver.widgets.Rectangle;
import androidx.core.content.ContextCompat;

import com.example.mygame.R;

public class HighlightAnimation extends CustomAnimation {
    /*
        HightlightAnimation : Children class of CustomAnimation representing an highlight animation on EditTexts
        - Attributes :
            Drawable oldDrawable        :  default Drawable of the View
        - Constructor :
            HighlightAnimation(View, int, long) :  Instances the class by calling super constructor and specifying the initial Drawable
        - Methods :
            void addViewListeners()             :  adds listeners on view to be reactive for the animation
            void configureValueAnimator()       :  prepares the animation to be played

     */
    private Drawable oldDrawable;
    public HighlightAnimation(View viewToHighlight, int colorDisplayed, long duration) {
        super(viewToHighlight, duration);
        viewToAnimate = viewToHighlight;
        animationDuration = duration;
        animation = ValueAnimator.ofObject(new ArgbEvaluator(), colorDisplayed, Color.TRANSPARENT);
        if(viewToHighlight != null) {
            oldDrawable = viewToHighlight.getBackground();
            configureValueAnimator();
            addViewListeners();
            viewToHighlight.setBackgroundTintList(ColorStateList.valueOf(colorDisplayed));
        }
    }

    private void addViewListeners() {
        viewToAnimate.setOnFocusChangeListener((v, hasFocus) -> { // When focus changes, disables the animation
            if(hasFocus) {
                v.setBackground(oldDrawable);
                v.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
            }
        });
        if(viewToAnimate instanceof  EditText) {
            final EditText editTextToAnimate = (EditText) viewToAnimate;
            editTextToAnimate.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) { // Or when text changes
                    editTextToAnimate.setBackground(oldDrawable);
                    editTextToAnimate.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
                }
                @Override
                public void afterTextChanged(Editable s) { }
            });
        }
    }
    private void configureValueAnimator()
    {
        ValueAnimator valueAnimator = (ValueAnimator) animation;
        valueAnimator.addUpdateListener(animator -> viewToAnimate.setBackgroundColor((int) animator.getAnimatedValue()));
        valueAnimator.addListener(new ValueAnimator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) { // Reset the old drawable
                viewToAnimate.setBackground(oldDrawable);
                viewToAnimate.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
}
