package com.example.mygame.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mygame.R;
import com.example.mygame.animations.CustomAnimationList;
import com.example.mygame.animations.HighlightAnimation;
import com.example.mygame.game.Player;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnRules = findViewById(R.id.btnRules);
        final Button btn_connect = findViewById(R.id.btn_register);
        final CustomAnimationList animationsList = new CustomAnimationList();
        final EditText et_login = findViewById(R.id.et_login), et_password = findViewById(R.id.et_password);
        TextView tv_register = findViewById(R.id.tv_connect);

        tv_register.setOnClickListener(v -> { // Go to RegisterActivity
            Intent newIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(newIntent);
            finish();
        });
        btnRules.setOnClickListener(v -> { // Go to Rules
            startActivity(new Intent(getApplicationContext(), RulesActivity.class));
        });
        btn_connect.setOnClickListener(v -> { // Tries to connect
            if (animationsList.hasEnded()) { // To prevent so loaded threads
                animationsList.getListAnimations().clear();
                    /*
                        et_toFixData : EditText to animate in case of its content is unvalidated
                     */
                    EditText et_toFixData = (et_login.getText().toString().isEmpty() ? et_login : et_password.getText().toString().isEmpty() ? et_password : null);
                    if(et_toFixData != null) {
                        animationsList.getListAnimations().add(new HighlightAnimation(et_toFixData, 0xBBBB0000, 250));
                        Toast.makeText(getApplicationContext(), "Veuillez vérifier votre login et votre mot de passe !", Toast.LENGTH_SHORT).show();
                    }
                    else if(!Player.isNetworkAvailable(LoginActivity.this))
                        Toast.makeText(getApplicationContext(), "Veuillez vérifier votre connexion !", Toast.LENGTH_SHORT).show();
                    else{
                        ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                                "Tentative de connexion...", true);
                        dialog.show();
                        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference("users/");
                        rootRef.orderByChild("login").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                DataSnapshot foundSnap = null;
                                for (DataSnapshot snap: snapshot.getChildren()) {
                                    if(snap.child("login").getValue().equals(et_login.getText().toString().trim()))
                                        foundSnap = snap;
                                }
                                System.out.println(foundSnap);
                                if(foundSnap != null && foundSnap.exists() && foundSnap.child("encodedPassword").getValue().equals(Player.encodePasswordSha512(et_password.getText().toString())))
                                {
                                    System.out.println(foundSnap.child("login").getValue(String.class));
                                    Toast.makeText(getApplicationContext(), "Connexion réussie !", Toast.LENGTH_SHORT).show();
                                    animationsList.getListAnimations().add(new HighlightAnimation((View) et_login.getParent(), Color.GREEN, 300));
                                    Player.createPlayerFromDatabase(getApplicationContext(), Long.parseLong(foundSnap.getKey()));
                                } else { // If username or password are uncorrect
                                    animationsList.getListAnimations().add(new HighlightAnimation((View) et_login.getParent(), Color.RED, 300));
                                    Toast.makeText(getApplicationContext(), "Connexion échouée", Toast.LENGTH_SHORT).show();
                                }
                                dialog.cancel();
                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(), "Une erreur est survenue. Vérifiez votre compte !", Toast.LENGTH_SHORT).show();
                                System.out.println(databaseError);
                            }
                        });
                    }
                animationsList.start();
                }
            });

    }
}