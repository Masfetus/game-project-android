package com.example.mygame.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mygame.R;
import com.example.mygame.animations.CustomAnimationList;
import com.example.mygame.animations.HighlightAnimation;
import com.example.mygame.game.Player;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final Button btn_connect = findViewById(R.id.btn_register);
        final Activity currentActivity = this;
        final CustomAnimationList animationsList = new CustomAnimationList();
        final EditText et_login = findViewById(R.id.et_login2), et_password = findViewById(R.id.et_password2), et_age = findViewById(R.id.et_age), et_pseudo = findViewById(R.id.et_pseudo);
        TextView tv_connect = findViewById(R.id.tv_connect);

        tv_connect.setOnClickListener(v -> { // Go to RegisterActivity !
            Intent newIntent = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(newIntent);
        });
        btn_connect.setOnClickListener(v -> {
            if (animationsList.hasEnded()) {
                animationsList.getListAnimations().clear();
                Toast toast_msgError = null;
                // EditText to animate in case of its content is unvalidated !
                EditText et_toFixData = (et_login.getText().toString().isEmpty() ? et_login : et_password.getText().toString().isEmpty() ? et_password : et_pseudo.getText().toString().isEmpty() ? et_pseudo :  et_age.getText().toString().isEmpty() ? et_age : null);
                byte age = 0;
                if(et_toFixData == null) {
                    if(et_pseudo.getText().toString().trim().length() > 16){
                        et_toFixData = et_pseudo;
                        toast_msgError = Toast.makeText(getApplicationContext(), "Votre pseudo doit comporter moins de 16 caractères !", Toast.LENGTH_SHORT);
                    }
                    else {
                        try {
                            age = Byte.parseByte(et_age.getText().toString());
                        } catch (NumberFormatException ex) {
                            age = -1;
                        } finally {
                            if (age <= 0 || age >= 130) {
                                toast_msgError = Toast.makeText(getApplicationContext(), "Votre âge semble très peu crédible !", Toast.LENGTH_SHORT);
                                et_toFixData = et_age;
                            }
                        }
                    }
                }
                if(et_toFixData != null) {
                    animationsList.getListAnimations().add(new HighlightAnimation(et_toFixData, 0xBBBB0000, 250));
                    if(toast_msgError == null)
                        toast_msgError = Toast.makeText(getApplicationContext(), "Veuillez vérifier tous les champs !", Toast.LENGTH_SHORT);
                    toast_msgError.show();
                    animationsList.start();
                }
                else if(!Player.isNetworkAvailable(currentActivity)) {
                    toast_msgError = Toast.makeText(getApplicationContext(), "Veuillez vérifier votre connexion !", Toast.LENGTH_SHORT);
                    toast_msgError.show();
                }
                else {
                    Player newPlayerToSave = new Player(age, et_pseudo.getText().toString().trim(), et_login.getText().toString().trim(), Player.encodePasswordSha512(et_password.getText().toString()));
                    trySavePlayer(newPlayerToSave);
                }
            }
        });

    }
    private void trySavePlayer(final Player player)
    {
        ProgressDialog dialog = ProgressDialog.show(RegisterActivity.this, "",
                "Tentative de connexion...", true);
        dialog.show();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference("users");
        rootRef.orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    player.setuID(dataSnapshot.getChildrenCount());
                    rootRef.child("" + player.getuID()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            if (snapshot.exists())
                                Toast.makeText(getApplicationContext(), "Le compte est déjà enregistré !", Toast.LENGTH_SHORT).show();
                            else {
                                player.save();
                                Toast.makeText(getApplicationContext(), "Vous êtes inscrit ! Connectez-vous !", Toast.LENGTH_SHORT).show();
                            }
                            dialog.cancel();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError);

            }
        });
    }
}
