package com.example.mygame.fragments.ranking;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mygame.R;
import com.example.mygame.activities.HomeActivity;
import com.example.mygame.game.Player;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RankingFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ranking, container, false);
    }
    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        final ArrayList<Player> players = new ArrayList<Player>();
        final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("users");
        Query usersRanking = myRef.orderByChild("bitCoins").limitToLast(15);
        usersRanking.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { players.add(0, dataSnapshot.getValue(Player.class)); }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        usersRanking.addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(Player player : players)
                    addPlayerToRankingDisplay((byte) (players.indexOf(player) + 1), player, ((HomeActivity) getContext()).findViewById(R.id.rankingLayout));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError);
            }
        });
    }
    private void addPlayerToRankingDisplay(byte rank, Player player, TableLayout rankingLayout)
    {
        // Loading ranking row from an XML model
        LayoutInflater inflater = getLayoutInflater();
        TableRow newRow = (TableRow) inflater.inflate(R.layout.ranking_row, null);

        ((TextView) newRow.findViewById(R.id.rankingUsername)).setText(rank + " - " + player);
        ((TextView) newRow.findViewById(R.id.rankingBitcoins)).setText("" + player.getBitCoins());

        rankingLayout.addView(newRow);
    }
}
