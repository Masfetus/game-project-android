package com.example.mygame.game;

import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryGame implements ISavable {
    /*
            Class HistoryGame : represents a game that has been played and has to be stored (implementing ISavable)
            - Attributes :
                int difficultyLevel :                   Difficulty Level between 1 and 5
                int reward      :                       Gain of the user after the game
                int scoreLooser, scoreWinner :          Both scores of the game (winner and looser)
                String winner   :                       Username of the winner
                String player1  :                       Username of the user
                Date date       :                       Date of the game
            - Constructors :
                HistoryGame(int, int, int, int, String, String) : instances the class with all the previous attributes passed and the current date
                HistoryGame()                                   : default constructor called by Firebase when it retrieves data
            - Getters :
                int getDifficultyLevel() :      returns value of difficultyLevel
                int getReward() :               returns value of reward
                Date getDate() :                returns value of date
                int getScoreLooser() :          returns value of scoreLooser
                int getScoreWinner() :          returns value of scoreWinner
                String getWinner()   :          returns value of winner
                String getPlayer1()  :          returns value of player1
            - Override methods :
                public void save() :            saves the game in Firebase
         */
    private int difficultyLevel;
    private int reward;
    private int scoreLooser;
    private int scoreWinner;
    private String winner;
    private String player1;
    private Date date;

    public HistoryGame(int difficultyLevel, int reward, int scoreLooser, int scoreWinner, String winner, String player1) {
        this.difficultyLevel = (difficultyLevel >= 1 && difficultyLevel <= 5 ? difficultyLevel : 1);
        this.reward = reward;
        this.scoreLooser = (scoreLooser >= 0 ? scoreLooser : 0);
        this.scoreWinner = (scoreWinner >= 0 ? scoreWinner : 0);
        this.winner = (winner != null ? winner : "Bot");
        this.player1 = (player1 != null ? player1 : "Bot");
        this.date = new Date();
    }
    public HistoryGame() // Used by Firebase to retrieve data
    {
        this(1, 0, 0, 0, null, null);
    }
    public int getDifficultyLevel() {
        return difficultyLevel;
    }

    public int getReward() {
        return reward;
    }
    public Date getDate() {
        return date;
    }
    public int getScoreLooser() {
        return scoreLooser;
    }

    public int getScoreWinner() {
        return scoreWinner;
    }

    public String getWinner() {
        return winner;
    }

    public String getPlayer1() {
        return player1;
    }

    public void save()
    {

        if(this != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); // Initializes the current Date
            FirebaseDatabase.getInstance().getReference("games/" + formatter.format(date)).setValue(this);
        }
    }
}
