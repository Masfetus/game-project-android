package com.example.mygame.fragments.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.mygame.R;
import com.example.mygame.activities.HomeActivity;
import com.example.mygame.game.HistoryGame;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class HistoryFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }
    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        final ArrayList<HistoryGame> games = new ArrayList<>();
        final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("games");
        Query usersRanking = myRef.orderByChild("player1").equalTo(HomeActivity.getCurrentPlayer().getPseudo()).limitToLast(20);
        usersRanking.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { games.add(0, dataSnapshot.getValue(HistoryGame.class)); }
            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) { }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
        usersRanking.addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(HistoryGame game : games){
                    addGameToHistoryDisplay(game);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError);
            }
        });
    }
    private void addGameToHistoryDisplay(HistoryGame historyGame)
    {
        // Loading an XML model of row in the history
        LayoutInflater inflater = getLayoutInflater();
        TableRow newRow = (TableRow) inflater.inflate(R.layout.game_row, null);
        TableLayout historyLayout = ((HomeActivity) getContext()).findViewById(R.id.historyLayout);
        if(!historyGame.getWinner().equals(HomeActivity.getCurrentPlayer().getPseudo()))
            newRow.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorBackgroundDefeat));
        ((TextView) newRow.findViewById(R.id.tvGameDate)).setText(new SimpleDateFormat("dd/MM/yyyy").format(historyGame.getDate()) + "\n"  +
                                                                    new SimpleDateFormat("HH:mm").format(historyGame.getDate()));
        if(historyGame.getPlayer1().equals(historyGame.getWinner())) {
            ((TextView) newRow.findViewById(R.id.tvGamePlayer1)).setText(historyGame.getPlayer1() + "\n" + historyGame.getScoreWinner());
            ((TextView) newRow.findViewById(R.id.tvGamePlayer2)).setText("Bot\n" + historyGame.getScoreLooser());
        }
        else{
            ((TextView) newRow.findViewById(R.id.tvGamePlayer1)).setText(historyGame.getPlayer1() + "\n" + historyGame.getScoreLooser());
            ((TextView) newRow.findViewById(R.id.tvGamePlayer2)).setText("Bot\n" + historyGame.getScoreWinner());
        }
        ((TextView) newRow.findViewById(R.id.tvReward)).setText((historyGame.getReward() > 0 ? "+" + historyGame.getReward() : + historyGame.getReward() ) + "\nNiv." + historyGame.getDifficultyLevel());
        historyLayout.addView(newRow);
    }
}
