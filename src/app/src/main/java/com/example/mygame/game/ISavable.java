package com.example.mygame.game;

/*
    Functional Interface ISavable :  represents an object stored in Firebase
    Required to implement method :
        - void save();      How the element is stored
 */
@FunctionalInterface
public interface ISavable {
    void save();
}
