package com.example.mygame.game;

import android.app.Activity;
import android.content.Intent;


import android.app.AlertDialog;

import com.example.mygame.activities.HomeActivity;
import com.example.mygame.activities.GameActivity;

import java.util.ArrayList;
import java.util.Random;

public class Game{

    /*
        Class Game : represents a game that user is playing
        - Attributes :
            Player[] players :              Array of Player storing both players (user and IA)
            Activity gameActivity :         Activity where the game takes place
            byte difficultyLevel :          Difficulty Level between 1 and 5
            byte currentStep :              Number of the round
            Player lastWinner :             Player who won the last round
        - Constants :
            byte STEPS_NUMBER       :       const representing number of maximum round
            byte[][] REWARDS        :       const storing all rewards depending on difficulty level (lines) and final result (columns)
            byte[][] WIN_RATES      :       const storing all the rates required to set the IA difficulty (defines how much kind of winning/loosing elements will be able to play)
            boolean[][] FIGHT_RULES :       const defining rules to know if element wins against another (To read as : Element in line X wins (or not) against element in column Y)
        - Constructors :
            Game(Player, Player, byte, Activity) : instance the class with both players, difficulty level and game activity
        - Getters :
            byte getDifficultyLevel() : returns value of difficultyLevel
            Player[] getPlayers() : returns value of players
            Player getLastWinner() : returns value of lastWinner
        - Methods :
            public void play(Player, Elements)      :   allows a player to play an element, by managing the round and the "winnerOfTheRound" return
            private Player winnerOfTheRound()       :   returns the winner of the round by respecting the fight rules and last elements played
            public void endGame(Player, Player)     :   ends the game by choosing first player as winner and the second one as looser
            public Elements getElementPlayedByBot() :   returns the element played by the bot after the player play according to WIN_RATES and difficulty level
     */
    private Player[] players;
    private static final byte STEPS_NUMBER = 5;
    private Activity gameActivity;
    private byte difficultyLevel;
    private byte currentStep;
    private Player lastWinner;

    private final byte[][] REWARDS = {
        //   V    D     Victoire ou Défaite
            {5, -30},  // Niv.1
            {10, -25},  // Niv.2
            {20, -20},  // Niv.3
            {30, -20},  // Niv.4
            {40, -20}   // Niv.5
    };
    private final byte[][] WIN_RATES = {
        //   V  D  N      (Victoire, Égalité, Défaite)
/* Niv.1 */ {3, 1, 1}, // Bot could play an element in a choice of 3 loosing elements, 1 winning and 1 same as user
/* Niv.2 */ {2, 1, 2}, // Bot could play an element in a choice of 2 loosing elements, 1 winning and 2 same as user
/* Niv.3 */ {2, 2, 1}, // Bot could play an element in a choice of 2 loosing elements, 2 winning and 1 same as user
/* Niv.4 */ {1, 2, 2}, // Bot could play an element in a choice of 1 loosing elements, 2 winning and 2 same as user
/* Niv.5 */ {1, 3, 1}, // Bot could play an element in a choice of 1 loosing elements, 3 winning and 1 same as user

    };
    private final boolean[][] FIGHT_RULES = {
            //Pierre  Feuille Ciseaux Lezard Spock
  /* Pierre */ {false, false, true,  true,  false }, // Rock wins against scissors and lizard
  /* Feuille */ {true,  false, false, false, true},   // Leaf wins against rock and spock
  /* Ciseau */ {false, true,  false, true,  false }, // Scissors win against leaf and lizard
  /* Lezard */ {false, true,  false, false, true  }, // Lizard wins against leaf and spock
  /* Spock */  {true,  false, true,  false, false }, // Spock wins against rock and scissors
    };


    public Game(Player player1, Player player2, byte difficultyLevel, Activity context){
        if(player1 != null){
            players = new Player[2];
            players[0] = player1;
        }
        if(player2 == null) // If second player is null so it's a bot
            players[1] = new Player(0, "Bot", null, null);
        currentStep = 1;
        for (Player player : players)
            player.setLastScore(0);
        this.difficultyLevel = difficultyLevel;
        gameActivity = (context != null ? context : new Activity());
        lastWinner = null;
    }

    public int getDifficultyLevel() { return difficultyLevel; }
    public Player[] getPlayers()    { return players;         }
    public Player getLastWinner()   { return lastWinner;      }

    public void play(Player player, Elements element){
        if(element != null && player != null) {
            player.setLastElementPlayed(element);
            if (players[0].getLastElementPlayed() != null && players[1].getLastElementPlayed() != null) { // If both players play in the current round
                Player winner = winnerOfTheRound(); // Let's get the winner
                if(winner != null){ // If there's a winner (no equalities)
                    winner.setLastScore(winner.getLastScore() + 1);
                    currentStep++;
                    if(winner.getLastScore() >= (STEPS_NUMBER / 2) + 1) { // When a player reached a winning score
                        Player looser = (winner == players[0] ? players[1] : players[0]);
                        endGame(winner, looser);
                    }
                }
                lastWinner = winner;
                players[0].setLastElementPlayed(null);
                players[1].setLastElementPlayed(null);
            }
        }
    }
    private Player winnerOfTheRound()
    {
        if(players[0].getLastElementPlayed() == players[1].getLastElementPlayed())
            return null;
        return (FIGHT_RULES[players[0].getLastElementPlayed().getValue()][players[1].getLastElementPlayed().getValue()] ? players[0] : players[1]);
    }
    public void endGame(Player winner, Player looser)
    {
        AlertDialog.Builder popup = new AlertDialog.Builder(gameActivity);
        popup.setTitle("Fin de la partie");
        byte reward = 0;
        if(winner == players[0]){
            reward = REWARDS[difficultyLevel-1][0];
            popup.setMessage("Vous avez gagné la partie.\nVous recevez " + reward + " bitcoins !");
        }
        else if(looser == players[0]) {
            reward = REWARDS[difficultyLevel - 1][1];
            popup.setMessage("Vous avez perdu la partie.\nVous perdez " + -reward + " bitcoins !");

        }
        popup.setPositiveButton("Rejouer", (dialog, which) -> {
            Intent intent = new Intent(gameActivity, GameActivity.class);
            intent.putExtra("player1", players[0]);
            intent.putExtra("difficultyLevel", (int) difficultyLevel);
            gameActivity.startActivity(intent);
            gameActivity.finish();

        });
        popup.setNegativeButton("Quitter", (dialog, which) -> {
            Intent intent = new Intent(gameActivity, HomeActivity.class);
            intent.putExtra("player", players[0]);
            gameActivity.startActivity(intent);
            gameActivity.finish();
        });
        popup.setCancelable(false);
        popup.show();

        // SAVES
        players[0].giveBitcoins(reward);
        players[0].save();
        new HistoryGame(difficultyLevel, reward, currentStep - winner.getLastScore() - 1, winner.getLastScore(), winner.getPseudo(), players[0].getPseudo()).save();
    }

    public Elements getElementPlayedByBot() {
        ArrayList<Elements> listPossiblePlays = new ArrayList<>();
        for(byte counter = 0; counter < 2; counter++){ // For both results (Victory and Defeat)
            byte numberElementsMatched = 0, elementIndex = 0;
            while(numberElementsMatched < WIN_RATES[difficultyLevel-1][counter]) { // Adding all matching elements until we respect the WIN_RATES
                boolean isElementMatching = FIGHT_RULES[elementIndex][players[0].getLastElementPlayed().getValue()];
                if ((isElementMatching && counter == 1 || !isElementMatching && counter == 0) && Elements.valueOf(elementIndex) != players[0].getLastElementPlayed()){ // Considering the element matches
                    listPossiblePlays.add(Elements.valueOf(elementIndex));
                    numberElementsMatched++;
                }
                elementIndex++;
                if (elementIndex == FIGHT_RULES.length) // Go back to the beginning
                    elementIndex = 0;
            }
        }
        for(byte numberOfSameElement = 0; numberOfSameElement < WIN_RATES[difficultyLevel-1][2]; numberOfSameElement++) // Add as much same element as mentioned in the WIN_RATES
            listPossiblePlays.add(players[0].getLastElementPlayed());
        return listPossiblePlays.get(new Random().nextInt(listPossiblePlays.size()));
    }
}
