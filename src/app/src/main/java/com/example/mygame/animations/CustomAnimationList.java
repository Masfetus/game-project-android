package com.example.mygame.animations;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class CustomAnimationList extends AbstractList<CustomAnimation> implements IStartable{
    /*
        CustomAnimationList : represents list of custom animations
        - Attributes :
            List<CustomAnimation>  listAnimations : List storing all the animations
        - Constructor :
            CustomAnimationList(List) :  Instances the class with the list passed
            CustomAnimationList()     :  Instances the class with a new empty list
        - Getter :
            List<CustomAnimation> getListAnimations() :  returns value of listAnimations
        - Override methods :
            CustomAnimation get(int):  returns the custom animation at the index mentioned
            int size()              :  returns the size of the list
            void start()           :   starts all the animations with their duration
        - Methods :
            boolean hasEnded()     :   checks if all animations in the list have ended

     */
    private List<CustomAnimation> listAnimations;

    public List<CustomAnimation> getListAnimations() {
        return listAnimations;
    }

    public CustomAnimationList(List<CustomAnimation> listAnimations){
        super();
        this.listAnimations = listAnimations;
    }
    public CustomAnimationList()
    {
        this(new ArrayList<>());
    }

    @Override
    public CustomAnimation get(int index) {
        return listAnimations.get(index);
    }

    @Override
    public int size() {
        return listAnimations.size();
    }

    @Override
    public void start()
    {
        for(CustomAnimation animationToLoad : listAnimations){
            animationToLoad.start();
        }
    }
    public boolean hasEnded()
    {
        for(CustomAnimation animationToLoad : listAnimations){
            if(animationToLoad.getAnimation().isRunning())
                return false;
        }
        return true;
    }

}
