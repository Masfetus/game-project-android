package com.example.mygame.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigator;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mygame.R;
import com.example.mygame.game.Elements;
import com.example.mygame.game.Game;
import com.example.mygame.game.Player;

public class GameActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private Game currentGame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        getSupportActionBar().hide();

        Intent intent = getIntent(); // Get the data from homeActivity
        int difficulty = intent.getIntExtra("difficultyLevel", 1);
        final Player player1 = (Player) intent.getSerializableExtra("player1");

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.music); // Starts a back music
        mediaPlayer.start();

        currentGame = new Game(player1, null, (byte) difficulty, this); // Let's start a new game

        updateActivity(null);

        // Configuration of all the button that user will press to play
        final ImageButton btnSpock = findViewById(R.id.btnSpock), btnLizard = findViewById(R.id.btnLizard),
                btnRock = findViewById(R.id.btnRock),
                btnLeaf = findViewById(R.id.btnPaper),
                btnScissors = findViewById(R.id.btnScissors);

        View.OnClickListener onClickListener = v -> {
            Elements botElement;
            currentGame.play(player1, Elements.valueOf(Integer.parseInt((String) v.getTag())));
            botElement = currentGame.getElementPlayedByBot();
            currentGame.play(currentGame.getPlayers()[1], botElement);
            updateActivity(botElement);
        };

        btnSpock.setOnClickListener(onClickListener);
        btnLizard.setOnClickListener(onClickListener);
        btnLeaf.setOnClickListener(onClickListener);
        btnRock.setOnClickListener(onClickListener);
        btnScissors.setOnClickListener(onClickListener);

        // Rules activity
        (findViewById(R.id.btnRules)).setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), RulesActivity.class));
        });
    }
    @Override
    public void onBackPressed(){
        AlertDialog.Builder popup = new AlertDialog.Builder(GameActivity.this);
        popup.setTitle("Fin de la partie ?");
        popup.setMessage("Êtes-vous sûr de vouloir mettre fin à la partie ?\nVous serez considéré comme perdant.");
        popup.setNegativeButton("Non", (dialog, which) -> {
            dialog.cancel();
        });
        popup.setPositiveButton("Oui", (dialog, which) -> {
            dialog.cancel();
            currentGame.endGame(currentGame.getPlayers()[1], currentGame.getPlayers()[0]);
        });
        popup.show();
    }
    private void updateActivity(Elements elementPlayed)
    {
        TextView[] tvPseudoPlayer = {findViewById(R.id.tvPseudoPlayer1), findViewById(R.id.tvPseudoPlayer2)};
        TextView[] tvScorePlayer = {findViewById(R.id.tvScorePlayer1), findViewById(R.id.tvScorePlayer2)};
        Player[] playersArray = currentGame.getPlayers();
        TextView tvLevel = findViewById(R.id.tvLevel);
        tvLevel.setText("Niv." + currentGame.getDifficultyLevel());
        for(byte numPlayer = 0; numPlayer < playersArray.length; numPlayer++) // Update the header
        {
            tvPseudoPlayer[numPlayer].setText(playersArray[numPlayer].getPseudo());
            tvScorePlayer[numPlayer].setText("" + playersArray[numPlayer].getLastScore());
        }
        if(elementPlayed != null) { // Update the bottom where infos on bot play are displayed
            TextView tvResult = findViewById(R.id.tvResult);
            ImageView ivElement = findViewById(R.id.ivElementBot);
            if (playersArray[0] == currentGame.getLastWinner()) {
                tvResult.setTextColor(getResources().getColor(R.color.colorBackgroundWin, null));
                tvResult.setText("Gagné !");
            } else if (currentGame.getLastWinner() == null) {
                tvResult.setTextColor(Color.WHITE);
                tvResult.setText("Égalité !");
            } else {
                tvResult.setTextColor(getResources().getColor(R.color.colorBackgroundDefeat, null));
                tvResult.setText("Perdu !");
            }
            ivElement.setBackground(getDrawable(getResources().getIdentifier(elementPlayed.toString().toLowerCase(), "drawable", getPackageName())));
            ivElement.setVisibility(View.VISIBLE);
            tvResult.setVisibility(View.VISIBLE);
            findViewById(R.id.tvBot).setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (mediaPlayer != null)
            mediaPlayer.pause();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(mediaPlayer != null) {
            mediaPlayer.start();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mediaPlayer != null)
            mediaPlayer.release();
    }
}
