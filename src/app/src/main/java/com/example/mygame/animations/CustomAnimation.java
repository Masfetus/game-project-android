package com.example.mygame.animations;

import android.animation.Animator;
import android.view.View;
import android.view.animation.Animation;

public class CustomAnimation extends Animation implements IStartable
{
    /*
        CustomAnimation : Parent class representing all the custom animations I did
        - Attributes :
            Animator animation     : Animator of the animation instance
            long animationDuration : Duration of the animation
            View viewToAnimate     : View to animate
        - Constructor :
            CustomAnimation(View, long) :  Instances the class with a View and duration long
        - Getter :
            Animator getAnimation()    :  returns value of animation
        - Setter :
            void setViewToAnimate(View):  sets the view to animate at the passed value
        - Override method :
            void start()           : starts the Animation with the animationDuration

     */
    protected Animator animation;
    protected long animationDuration;
    protected View viewToAnimate;
    public CustomAnimation(View viewToAnimate, long animationDuration)
    {
        super();
        setViewToAnimate(viewToAnimate);
        setDuration(animationDuration);
    }
    @Override
    public void start()
    {
        if(animation != null) {
            animation.setDuration(animationDuration);
            animation.start();
        }
    }

    public void setViewToAnimate(View viewToAnimate) {
        this.viewToAnimate = viewToAnimate;
    }
    public Animator getAnimation() { return animation; }
}
